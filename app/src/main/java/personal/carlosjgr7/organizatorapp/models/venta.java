package personal.carlosjgr7.organizatorapp.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import personal.carlosjgr7.organizatorapp.app.configRealm;

public class venta extends RealmObject {
    @PrimaryKey
    private int id;
    private cliente cliente;
    private Date fechaVenta;
    private String fechaVentaString;

    public venta(){}

    public venta( cliente cliente) {

        this.id = configRealm.ventaID.incrementAndGet();
        this.cliente = cliente;
        this.fechaVenta = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        fechaVentaString = dateFormat.format( this.fechaVenta);


    }

    public int getId() {
        return id;
    }

    public Date getFechaVenta() {
        return fechaVenta;
    }

    public cliente getCliente() {
        return cliente;
    }

    public void setCliente(personal.carlosjgr7.organizatorapp.models.cliente cliente) {
        this.cliente = cliente;
    }


}
