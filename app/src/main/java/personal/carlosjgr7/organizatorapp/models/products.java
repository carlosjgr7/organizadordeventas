package personal.carlosjgr7.organizatorapp.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import personal.carlosjgr7.organizatorapp.app.configRealm;

public class products extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String codigo;

    public products() {  }

    public products(String name,String codigo) {
        this.id = configRealm.productsID.incrementAndGet();
        this.name = name;
        this.codigo=codigo;

    }


    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }

    public String getName() { return name;}

    public void setName(String name) {  this.name = name;}


}
