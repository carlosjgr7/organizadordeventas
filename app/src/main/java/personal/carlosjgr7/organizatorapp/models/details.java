package personal.carlosjgr7.organizatorapp.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import personal.carlosjgr7.organizatorapp.app.configRealm;

public class details extends RealmObject {
    @PrimaryKey
    private int id;
    private int idventa;
    private products products;
    private String unidad;
    private int cantProd;


    public details() { }



    public details(int cantProd, products products, String unidad, int idventa) {
        this.id = configRealm.detailID.incrementAndGet();
        this.products = products;
        this.cantProd = cantProd;
        this.unidad = unidad;
        this.idventa = idventa;
    }

    public String getUnidad() {  return unidad;}

    public void setUnidad(String unidad) { this.unidad = unidad;}

    public int getCantProd() {
        return cantProd;
    }

    public void setCantProd(int cantProd) {
        this.cantProd = cantProd;
    }

    public products getProducts() {
        return products;
    }

    public void setProducts(products products) {
        this.products = products;
    }

    public int getIdventa() {
        return idventa;
    }

    public void setIdventa(int idventa) {
        this.idventa = idventa;
    }

    public int getId() {
        return id;
    }


}
