package personal.carlosjgr7.organizatorapp.models;

import java.util.concurrent.atomic.AtomicInteger;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import personal.carlosjgr7.organizatorapp.app.configRealm;

public class cliente extends RealmObject {

    @PrimaryKey
    private int id;
    private String codigo;
    private String name;

    public cliente() { }


    public cliente( String name,String codigo) {
        this.id = configRealm.clientID.incrementAndGet();
        this.name = name;
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
