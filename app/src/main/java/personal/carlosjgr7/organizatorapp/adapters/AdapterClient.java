package personal.carlosjgr7.organizatorapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.models.cliente;

public class AdapterClient extends RecyclerView.Adapter<AdapterClient.viewHolder> {
    private RealmResults<cliente> Arrayclientes;
    private OnItemClickListener listener;
    private int layout;
    private Context context;

    public AdapterClient(RealmResults<cliente> clientes, int layout, OnItemClickListener listener) {
        this.Arrayclientes = clientes.sort("name", Sort.ASCENDING);
        this.listener = listener;
        this.layout = layout;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(this.layout,null,false);
        context = parent.getContext();
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.bind(this.Arrayclientes.get(position),this.listener);
    }

    @Override
    public int getItemCount() {
        return Arrayclientes.size();
    }




    public class viewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{
         TextView textView;


        public viewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textViewClientName);
            itemView.setOnCreateContextMenuListener(this);
        }

        public void bind(final cliente cliente, final OnItemClickListener listener) {
            textView.setText(cliente.getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClick(cliente,getAdapterPosition());
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {


            menu.setHeaderTitle(Arrayclientes.get(getAdapterPosition()).getName());
            menu.add("Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    cliente clientErase;
                   Realm realm = Realm.getDefaultInstance();
                   realm.executeTransaction(new Realm.Transaction() {
                       @Override
                       public void execute(Realm realm) {
                           RealmResults<cliente> realmResults = realm.where(cliente.class).equalTo("id",Arrayclientes.get(getAdapterPosition()).getId()).findAll();
                           realmResults.deleteAllFromRealm();
                       }
                   });
                    notifyItemRemoved(getAdapterPosition());
                    return false;
                }
            });
        }
    }

    public interface OnItemClickListener{
        public void OnClick(cliente cliente,int position);
    }
}
