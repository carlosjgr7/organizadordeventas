package personal.carlosjgr7.organizatorapp.adapters;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.models.details;


public class AdapterDetail extends RecyclerView.Adapter<AdapterDetail.viewHolder> {
    private RealmResults<details> Arraydetail;
    private OnItemClickListener listener;
    private int layout;
    private Context context;

    public AdapterDetail(RealmResults<details> details, int layout, OnItemClickListener listener) {
        this.Arraydetail = details;
        this.listener = listener;
        this.layout = layout;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(this.layout,null,false);
        context = parent.getContext();
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.bind(this.Arraydetail.get(position),this.listener);
    }

    @Override
    public int getItemCount() {
        return Arraydetail.size();
    }




    public class viewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView textView_producto;
        TextView textView_cantidad;
        TextView textView_unidad;



        public viewHolder(@NonNull View itemView) {
            super(itemView);
            textView_producto = itemView.findViewById(R.id.textViewPedidosDetail);
            textView_cantidad = itemView.findViewById(R.id.textViewCantidad);
            textView_unidad = itemView.findViewById(R.id.textViewUnd);
            itemView.setOnCreateContextMenuListener(this);

        }

        public void bind(final details details, final OnItemClickListener listener) {


            textView_producto.setText(details.getProducts().getName());
            textView_cantidad.setText(String.valueOf(details.getCantProd()));
            textView_unidad.setText(details.getUnidad());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClick(details,getAdapterPosition());
                }
            });
        }



            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {


                menu.setHeaderTitle(Arraydetail.get(getAdapterPosition()).getProducts().getName());
                menu.add("Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Realm realm = Realm.getDefaultInstance();

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmResults<details> realmResults = realm.where(details.class).equalTo("id",Arraydetail.get(getAdapterPosition()).getId()).findAll();
                                realmResults.deleteAllFromRealm();
                            }
                        });
                        notifyItemRemoved(getAdapterPosition());
                        return false;
                    }
                });
            }
        }

    public interface OnItemClickListener{
        public void OnClick(details detail, int position);
    }
}
