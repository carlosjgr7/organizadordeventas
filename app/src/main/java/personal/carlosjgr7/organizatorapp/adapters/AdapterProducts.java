package personal.carlosjgr7.organizatorapp.adapters;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.models.products;

public class AdapterProducts extends RecyclerView.Adapter<AdapterProducts.viewHolder> {
    private RealmResults<products> Arrayproducts;
    private OnItemClickListener listener;
    private int layout;
    private Context context;

    public AdapterProducts(RealmResults<products> products, int layout, OnItemClickListener listener) {
        this.Arrayproducts = products.sort("name", Sort.ASCENDING);
        this.listener = listener;
        this.layout = layout;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(this.layout,null,false);
        context = parent.getContext();
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.bind(this.Arrayproducts.get(position),this.listener);
    }

    @Override
    public int getItemCount() {
        return Arrayproducts.size();
    }




    public class viewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{
         TextView textView;


        public viewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textViewProductsName);
            itemView.setOnCreateContextMenuListener(this);
        }

        public void bind(final products products, final OnItemClickListener listener) {
            textView.setText(products.getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClick(products,getAdapterPosition());
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {


            menu.setHeaderTitle(Arrayproducts.get(getAdapterPosition()).getName());
            menu.add("Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                   Realm realm = Realm.getDefaultInstance();

                   realm.executeTransaction(new Realm.Transaction() {
                       @Override
                       public void execute(Realm realm) {
                           RealmResults<products> realmResults = realm.where(products.class).equalTo("id",Arrayproducts.get(getAdapterPosition()).getId()).findAll();
                           realmResults.deleteAllFromRealm();
                       }
                   });
                    notifyItemRemoved(getAdapterPosition());
                    return false;
                }
            });
        }
    }

    public interface OnItemClickListener{
        public void OnClick(products cliente, int position);
    }
}
