package personal.carlosjgr7.organizatorapp.adapters;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.models.venta;
import personal.carlosjgr7.organizatorapp.models.venta;

public class AdapterPedidos extends RecyclerView.Adapter<AdapterPedidos.viewHolder> {
    private RealmResults<venta> Arrayventas;
    private OnItemClickListener listener;
    private int layout;
    private Context context;

    public AdapterPedidos(RealmResults<venta> ventas, int layout, OnItemClickListener listener) {
        this.Arrayventas = ventas.sort("fechaVenta", Sort.DESCENDING);
        this.listener = listener;
        this.layout = layout;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(this.layout,null,false);
        context = parent.getContext();
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.bind(this.Arrayventas.get(position),this.listener);
    }

    @Override
    public int getItemCount() {
        return Arrayventas.size();
    }




    public class viewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{
        TextView textView_pedidos;
        TextView textView_Date;



        public viewHolder(@NonNull View itemView) {
            super(itemView);
            textView_pedidos = itemView.findViewById(R.id.textViewPedidosName);
            textView_Date = itemView.findViewById(R.id.textViewPedidosDate);
            itemView.setOnCreateContextMenuListener(this);
        }

        public void bind(final venta venta, final OnItemClickListener listener) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

            textView_Date.setText(dateFormat.format(venta.getFechaVenta()));

            textView_pedidos.setText(venta.getCliente().getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClick(venta,getAdapterPosition());
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {


            menu.setHeaderTitle(Arrayventas.get(getAdapterPosition()).getCliente().getName());
            menu.add("Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                   Realm realm = Realm.getDefaultInstance();

                   realm.executeTransaction(new Realm.Transaction() {
                       @Override
                       public void execute(Realm realm) {
                           RealmResults<venta> realmResults = realm.where(venta.class).equalTo("id",Arrayventas.get(getAdapterPosition()).getId()).findAll();
                           realmResults.deleteAllFromRealm();
                       }
                   });
                    notifyItemRemoved(getAdapterPosition());
                    return false;
                }
            });
        }
    }

    public interface OnItemClickListener{
        public void OnClick(venta venta, int position);
    }
}
