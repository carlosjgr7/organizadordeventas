package personal.carlosjgr7.organizatorapp.app;

import android.app.Application;

import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import personal.carlosjgr7.organizatorapp.models.cliente;
import personal.carlosjgr7.organizatorapp.models.details;
import personal.carlosjgr7.organizatorapp.models.products;
import personal.carlosjgr7.organizatorapp.models.venta;

public class configRealm extends Application {

    public static AtomicInteger productsID = new AtomicInteger();
    public static AtomicInteger ventaID = new AtomicInteger();
    public static AtomicInteger detailID = new AtomicInteger();
    public static AtomicInteger clientID = new AtomicInteger();

    @Override
    public void onCreate() {
        super.onCreate();
        setupConfig();
        Realm realm = Realm.getDefaultInstance();

        productsID = getIdByTable(realm, products.class);
        ventaID = getIdByTable(realm, venta.class);
        detailID = getIdByTable(realm, details.class);
        clientID = getIdByTable(realm, cliente.class);
        realm.close();

    }

    private<T extends RealmObject> AtomicInteger getIdByTable(Realm realm, Class<T> anyclass) {
        RealmResults<T> results = realm.where(anyclass).findAll();
        return (results.size()>0)?new AtomicInteger(results.max("id").intValue()):new AtomicInteger();
    }

    private void setupConfig() {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

}
