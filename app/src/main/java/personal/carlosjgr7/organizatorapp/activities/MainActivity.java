package personal.carlosjgr7.organizatorapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation.ClientFragment;
import personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation.DetailFragment;
import personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation.PedidosFragment;
import personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation.ProductsFragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Switch;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements PedidosFragment.OnFragmentInteractionListener{
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView =findViewById(R.id.nav_view);

        setDefaultValues();
        changeFragment(new PedidosFragment(),navigationView.getMenu().getItem(0),
                navigationView.getMenu().getItem(0).getTitle().toString());



        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Boolean transaccion =false;
                Fragment fragment = null;

                switch(menuItem.getItemId()){
                    case R.id.menu_cliente:
                        fragment = new ClientFragment();
                        transaccion = true;
                        break;

                    case R.id.menu_pedidos:
                        fragment = new PedidosFragment();
                        transaccion = true;
                        break;

                    case R.id.menu_productos:
                        fragment = new ProductsFragment();
                        transaccion = true;
                        break;
                }
                if (transaccion==true){
                    changeFragment(fragment,menuItem,menuItem.getTitle().toString());
                    drawerLayout.closeDrawers();
                }

                return false;
            }
        });

    }

    private void changeFragment(Fragment fragment, MenuItem menuItem,String title) {
        getSupportFragmentManager()
        .beginTransaction()
        .replace(R.id.content_frame,fragment)
        .commit();
        menuItem.setChecked(true);
        getSupportActionBar().setTitle(title);

    }

    private void setDefaultValues() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_hamburguer_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            changeFragment(new PedidosFragment(),navigationView.getMenu().getItem(0),
                    navigationView.getMenu().getItem(0).getTitle().toString());
        } else {

            super.onBackPressed();
        }
    }

    @Override
    public void onFragmentInteraction(int id) {
        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setVentaID(id);
        String detail= "Detalles De Pedido";
        changeFragment(detailFragment,navigationView.getMenu().getItem(0),detail);
    }
}
