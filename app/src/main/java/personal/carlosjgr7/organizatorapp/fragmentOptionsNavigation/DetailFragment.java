package personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import javax.security.auth.login.LoginException;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.adapters.AdapterDetail;
import personal.carlosjgr7.organizatorapp.models.details;
import personal.carlosjgr7.organizatorapp.models.products;
import personal.carlosjgr7.organizatorapp.models.venta;


public class DetailFragment extends Fragment implements View.OnClickListener{

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerdetails;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterDetail adapterDetails;
    private FloatingActionButton floatingActionButton;
    private Button aceptar;
    private Button cancelar;
    private AlertDialog alertDialog;
    private Realm realm;
    private int sizeOfDetails;
    private Spinner spinner;
    private RealmResults<details> realmresultdetail;
    private int ventaID;
    private EditText edtCant;
    private EditText edtUnd;

    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        
        final View view = inflater.inflate(R.layout.fragment_detail, container, false);

        layoutManager = new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);

        recyclerdetails = view.findViewById(R.id.recycler_detail);
        recyclerdetails.setHasFixedSize(true);
        recyclerdetails.setItemAnimator(new DefaultItemAnimator());
        recyclerdetails.setLayoutManager(layoutManager);


        realm = Realm.getDefaultInstance();

        realmresultdetail = realm.where(details.class).equalTo("idventa",ventaID).findAll();


        this.sizeOfDetails = realmresultdetail.size();

        adapterDetails = new AdapterDetail(realmresultdetail, R.layout.detalle, new AdapterDetail.OnItemClickListener() {
            @Override
            public void OnClick(details detail, int position) {

            }
        });
                recyclerdetails.setAdapter(adapterDetails);

        floatingActionButton = view.findViewById(R.id.floatingActionButtonDetail);
        floatingActionButton.setOnClickListener(this);
        return view;
    }

    public void setVentaID(int id){
        this.ventaID = id;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mListener = (OnFragmentInteractionListener) context;
        } catch (Exception e){
           e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if(view==this.floatingActionButton) {
        New_ProductDialog();
        }

        if(view == this.aceptar){
            Boolean notNullEdtx = findNLL();
            if(notNullEdtx == false){
                Toast.makeText(getContext(), "no puede agg productos con campos vacios", Toast.LENGTH_SHORT).show();
            }else {

                final products[] result = new products[1];
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        result[0] = realm.where(products.class).equalTo("name",spinner.getSelectedItem().toString()).findFirst();
                    }
                });
                details nuevo_detalle_venta = new details(Integer.parseInt(edtCant.getText().toString()),
                        result[0],edtUnd.getText().toString(),ventaID);

                aggDetailToDataBase(nuevo_detalle_venta);
                this.alertDialog.cancel();

            }

        }


        if(view==this.cancelar){
            this.alertDialog.cancel();

        }


    }

    private void aggDetailToDataBase(final details nuevo_detalle_venta) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(nuevo_detalle_venta);
            }
        });
    }

    private Boolean findNLL() {
        if((edtUnd.getText().toString().isEmpty())||(edtCant.getText().toString().isEmpty())){
            return false;
        }
        return true;
    }

    private void New_ProductDialog() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Agg Producto");

        View v = LayoutInflater.from(getContext())
                .inflate(R.layout.agg_detail_fab,null);
        builder.setView(v);

        this.spinner = v.findViewById(R.id.spiner_select_product_detail);
        this.aceptar = v.findViewById(R.id.btn_add_new_detail);
        this.cancelar = v.findViewById(R.id.btn_cancelar_detail);
        this.edtCant = v.findViewById(R.id.editTextcant);
        this.edtUnd = v.findViewById(R.id.editTextund);

        loadSpiner();

        aceptar.setOnClickListener(this);
        cancelar.setOnClickListener(this);

        this.alertDialog = builder.create();
        this.alertDialog.show();



    }

    private void loadSpiner() {

        RealmResults<products>  realmSpin;
        ArrayList<String> namesOfProducts=new ArrayList<>();
        realmSpin = realm.where(products.class).findAll();
        realmSpin = realmSpin.sort("name", Sort.ASCENDING);

        for ( products e:realmSpin) {
            namesOfProducts.add(e.getName());
        }
        if(namesOfProducts.size()==0){
            namesOfProducts.add("No tienes clientes agregados");
            this.aceptar.setVisibility(View.GONE);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_dropdown_item
                , namesOfProducts);
        this.spinner.setAdapter(adapter);

    }

    
    
    
    private void DataChange() {
        adapterDetails.notifyDataSetChanged();
        recyclerdetails.scrollToPosition(realmresultdetail.size()-1);
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
