package personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.util.ArrayList;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.adapters.AdapterClient;
import personal.carlosjgr7.organizatorapp.app.util;
import personal.carlosjgr7.organizatorapp.models.cliente;


public class ClientFragment extends Fragment implements View.OnClickListener, RealmChangeListener<RealmResults<cliente>> {


    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerclient;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterClient adapterClient;
    private FloatingActionButton floatingActionButton;
    private Button aceptar;
    private Button cancelar;
    private EditText nueva_empresa;
    private AlertDialog alertDialog;
    private Realm realm;
    private int sizeOfClient;
    private TextView code;
    private RealmResults<cliente>  realmResults;

    public ClientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_client,container,false);

        layoutManager = new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);

        recyclerclient = view.findViewById(R.id.recycler_client);
        recyclerclient.setHasFixedSize(true);
        recyclerclient.setItemAnimator(new DefaultItemAnimator());
        recyclerclient.setLayoutManager(layoutManager);

        realm = Realm.getDefaultInstance();

        realmResults = realm.where(cliente.class).findAll();
        realmResults.addChangeListener(this);
        this.sizeOfClient = realmResults.size();






        adapterClient = new AdapterClient(realmResults, R.layout.cliente, new AdapterClient.OnItemClickListener() {
            @Override
            public void OnClick(cliente cliente, int position) {

            }
        });

        recyclerclient.setAdapter(adapterClient);

        floatingActionButton = view.findViewById(R.id.floatingActionButtonClient);
        floatingActionButton.setOnClickListener(this);

        if (realmResults.size()<util.RANGO_MENOR)
        clientesDefoult();


        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
     try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (Exception e){
           e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

        if(view==this.floatingActionButton){
                createNewClientDoalog();
        }
        if(view == this.aceptar){
            Boolean findNull = findNLL();
            if(findNull == false){
                Toast.makeText(getContext(), "Datos imcompletos para crear un cliente",
                        Toast.LENGTH_SHORT).show();

            }else {
                cliente nuevo_cliente = new cliente(this.nueva_empresa.getText().toString(),this.code.getText().toString());
                aggClientToDataBase(nuevo_cliente);
                this.alertDialog.cancel();
            }
        }

        if(view==this.cancelar){
            this.alertDialog.cancel();
        }
    }

    private Boolean findNLL() {
        if((nueva_empresa.getText().toString().isEmpty())||(code.getText().toString().isEmpty())){
            return false;
        }
        return true;
    }

    private void aggClientToDataBase(final cliente nuevo_cliente) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(nuevo_cliente);
            }

        });

    }

    private void clientesDefoult(){
        ArrayList<cliente> clientesDefoult= new ArrayList<cliente>();
        util.clientesXdefecto(clientesDefoult);
        Log.d("Clientes", clientesDefoult.toString());
        for (cliente cliente: clientesDefoult) {
            aggClientToDataBase(cliente);

        }

    }



    private void createNewClientDoalog() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Nuevo Cliente");

        View v = LayoutInflater.from(getContext())
                .inflate(R.layout.agg_client_fab,null);
        builder.setView(v);

        this.nueva_empresa = v.findViewById(R.id.edt_new_client);
        this.aceptar = v.findViewById(R.id.btn_add_new_client);
        this.cancelar = v.findViewById(R.id.btn_cancelar);
        this.code = v.findViewById(R.id.editTextcode);


        aceptar.setOnClickListener(this);
        cancelar.setOnClickListener(this);

        this.alertDialog = builder.create();
        this.alertDialog.show();
    }

    @Override
    public void onChange(RealmResults<cliente> clientes) {
        if (this.sizeOfClient<realmResults.size()){
            adapterClient.notifyDataSetChanged();
            recyclerclient.scrollToPosition(realmResults.size()-1);
            sizeOfClient = realmResults.size();

        }
        sizeOfClient = realmResults.size();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String string);
    }
}
