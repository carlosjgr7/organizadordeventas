package personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.adapters.AdapterProducts;
import personal.carlosjgr7.organizatorapp.app.util;
import personal.carlosjgr7.organizatorapp.models.products;


public class ProductsFragment extends Fragment implements View.OnClickListener, RealmChangeListener<RealmResults<products>> {

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerproducts;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterProducts adapterProducts;
    private FloatingActionButton floatingActionButton;
    private Button aceptar;
    private Button cancelar;
    private EditText nueva_empresa;
    private EditText code;
    private AlertDialog alertDialog;
    private Realm realm;
    private int sizeOfProducts;
    private RealmResults<products>  realmResults;


    public ProductsFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_products, container, false);

        layoutManager = new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);

        recyclerproducts = view.findViewById(R.id.recycler_client);
        recyclerproducts.setHasFixedSize(true);
        recyclerproducts.setItemAnimator(new DefaultItemAnimator());
        recyclerproducts.setLayoutManager(layoutManager);

        realm = Realm.getDefaultInstance();

        realmResults = realm.where(products.class).findAll();
        realmResults.addChangeListener(this);
        this.sizeOfProducts = realmResults.size();



        adapterProducts = new AdapterProducts(realmResults, R.layout.producto, new AdapterProducts.OnItemClickListener() {
            @Override
            public void OnClick(products products, int position) {

            }
        });

        recyclerproducts.setAdapter(adapterProducts);

        floatingActionButton = view.findViewById(R.id.floatingActionButtonClient);
        floatingActionButton.setOnClickListener(this);

        if(sizeOfProducts<util.RANGO_MENOR)
            ProductsDefoult();


        return view;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       try {
           mListener = (OnFragmentInteractionListener) context;
       }catch (Exception e){
           e.printStackTrace();
         }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if(view==this.floatingActionButton){
            createNewProductDoalog();
        }
        if(view == this.aceptar){
            Boolean notNullEdtx = findNLL();
            if(notNullEdtx == false) {
                Toast.makeText(getContext(), "no puede agg productos con campos vacios", Toast.LENGTH_SHORT).show();
            }else {
                products nuevo_products = new products(this.nueva_empresa.getText().toString(),code.getText().toString());
                aggProductToDataBase(nuevo_products);
                this.alertDialog.cancel();
            }
        }

        if(view==this.cancelar){
            this.alertDialog.cancel();
        }
    }


    private Boolean findNLL() {
        if((nueva_empresa.getText().toString().isEmpty())||(code.getText().toString().isEmpty())){
            return false;
        }
        return true;
    }


    private void ProductsDefoult(){
        ArrayList<products> products= new ArrayList<products>();
        util.ProductsXdefecto(products);
        for (products product: products) {
            aggProductToDataBase(product);

        }
    }

    private void aggProductToDataBase(final products nuevo_products) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(nuevo_products);
            }

        });

    }

    private void createNewProductDoalog() {

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Nuevo Producto");

        View v = LayoutInflater.from(getContext())
                .inflate(R.layout.agg_product_fab,null);
        builder.setView(v);

        this.nueva_empresa = v.findViewById(R.id.edt_new_product);
        this.aceptar = v.findViewById(R.id.btn_add_new_product);
        this.cancelar = v.findViewById(R.id.btn_cancelar_product);
        this.code = v.findViewById(R.id.textViewCode);

        aceptar.setOnClickListener(this);
        cancelar.setOnClickListener(this);

        this.alertDialog = builder.create();
        this.alertDialog.show();



    }

    @Override
    public void onChange(RealmResults<products> products) {
        if (this.sizeOfProducts<realmResults.size()){
            adapterProducts.notifyDataSetChanged();
            recyclerproducts.scrollToPosition(realmResults.size()-1);
            sizeOfProducts = realmResults.size();

        }
        sizeOfProducts = realmResults.size();
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String string);
    }
}
