package personal.carlosjgr7.organizatorapp.fragmentOptionsNavigation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import personal.carlosjgr7.organizatorapp.R;
import personal.carlosjgr7.organizatorapp.activities.MainActivity;
import personal.carlosjgr7.organizatorapp.adapters.AdapterPedidos;
import personal.carlosjgr7.organizatorapp.adapters.AdapterProducts;
import personal.carlosjgr7.organizatorapp.models.cliente;
import personal.carlosjgr7.organizatorapp.models.details;
import personal.carlosjgr7.organizatorapp.models.venta;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class PedidosFragment extends Fragment implements View.OnClickListener, RealmChangeListener<RealmResults<venta>> {

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerclient;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterPedidos adapterPedidos;
    private FloatingActionButton floatingActionButton;
    private Button aceptar;
    private Button cancelar;
    private AlertDialog alertDialog;
    private Realm realm;
    private int sizeOfPedidos;
    private Spinner spinner;
    private RealmResults<venta> realmResults;
    private Button enviarCorreo;





    public PedidosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_pedidos, container, false);

        layoutManager = new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);


        recyclerclient = view.findViewById(R.id.recycler_pedidos);
        recyclerclient.setHasFixedSize(true);
        recyclerclient.setItemAnimator(new DefaultItemAnimator());
        recyclerclient.setLayoutManager(layoutManager);

        realm = Realm.getDefaultInstance();

        realmResults = realm.where(venta.class).findAll();
        realmResults.addChangeListener(this);
        this.sizeOfPedidos = realmResults.size();



        adapterPedidos = new AdapterPedidos(realmResults, R.layout.pedidos, new AdapterPedidos.OnItemClickListener() {
            @Override
            public void OnClick(venta venta, int position) {
                nexToDetail(venta.getId());

            }
        });


        recyclerclient.setAdapter(adapterPedidos);

        enviarCorreo = view.findViewById(R.id.btn_Imprimir);

        floatingActionButton = view.findViewById(R.id.floatingActionButtonPedidos);
        floatingActionButton.setOnClickListener(this);
        enviarCorreo.setOnClickListener(this);


        return view;
    }

    private void nexToDetail(int id) {
        mListener.onFragmentInteraction(id);
    }


    @Override
    public void onClick(View view) {
        if(view==this.floatingActionButton){
            createNewPedidosDoalog();
        }
        if(view == this.aceptar){
            final cliente[] result = new cliente[1];

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    result[0] = realm.where(cliente.class).equalTo("name",spinner.getSelectedItem().toString()).findFirst();
                }
            });
                venta nueva_venta = new venta(result[0]);
                aggVentaToDataBase(nueva_venta);
                this.alertDialog.cancel();
         }

        if(view == enviarCorreo){

            Date hoy = new Date();
            String prepareToEmail="";
            RealmResults<venta> pedidosOnDay;
            RealmResults<details> details=null;
            cliente Cliente;

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String date = dateFormat.format(hoy);
            
            try {
                pedidosOnDay = realm.where(venta.class).equalTo("fechaVentaString",date).findAll();

                if (pedidosOnDay.size() > 0) {
                    for(int i=0;i<pedidosOnDay.size();i++){
                        prepareToEmail+="****"+pedidosOnDay.get(i).getCliente().getName()+" "+pedidosOnDay.get(i).getCliente().getCodigo()+"***"+
                                System.getProperty("line.separator");
                        details = realm.where(details.class).equalTo("idventa", pedidosOnDay.get(i).getId()).findAll();
                        for(int j=0;j<details.size();j++){
                            prepareToEmail+=""+details.get(j).getCantProd()
                                    +" "+details.get(j).getUnidad()+" "
                                    +details.get(j).getProducts().getName()
                                    +" "+details.get(j).getProducts().getCodigo()
                                    + System.getProperty("line.separator");

                        }
                        prepareToEmail+=System.getProperty("line.separator");

                    }
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, "carlosjgr7@gmail.com");
                    emailIntent.putExtra(Intent.EXTRA_CC, "");
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Reporte del dia de Kariangellys Cabeza");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, prepareToEmail);

                    try {
                        startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                    } catch (android.content.ActivityNotFoundException ex) {

                    }

                }else {
                    Toast.makeText(getContext(), "No hay pedidos para hoy", Toast.LENGTH_SHORT).show();
                }
                
            }catch (Exception e){
                Toast.makeText(getContext(), "Aun no ha agregado pedidos", Toast.LENGTH_SHORT).show();

            }
          

            
        }


        if(view==this.cancelar){
            this.alertDialog.cancel();

        }

    }

    private void aggVentaToDataBase(final venta nueva_venta) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(nueva_venta);
            }

        });

    }

    private void createNewPedidosDoalog() {

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Nuevo Pedido");

        View v = LayoutInflater.from(getContext())
                .inflate(R.layout.agg_pedido_fab,null);
        builder.setView(v);

        this.spinner = v.findViewById(R.id.spiner_select_client);
        this.aceptar = v.findViewById(R.id.btn_add_new_pedido);
        this.cancelar = v.findViewById(R.id.btn_cancelar_pedido);

        loadSpiner();

        aceptar.setOnClickListener(this);
        cancelar.setOnClickListener(this);

        this.alertDialog = builder.create();
        this.alertDialog.show();

    }

    private void loadSpiner() {
        RealmResults<cliente>  realmSpin;
        ArrayList<String> namesOfClients=new ArrayList<>();
        realmSpin = realm.where(cliente.class).findAll();
        realmSpin = realmSpin.sort("name", Sort.ASCENDING);

        for (cliente e:realmSpin) {
            namesOfClients.add(e.getName());
        }
        if(namesOfClients.size()==0){
            namesOfClients.add("No tienes clientes agregados");
            this.aceptar.setVisibility(View.GONE);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_dropdown_item
                , namesOfClients);
        this.spinner.setAdapter(adapter);


    }

    @Override
    public void onChange(RealmResults<venta> ventas) {
        if (this.sizeOfPedidos<realmResults.size()){
            adapterPedidos.notifyDataSetChanged();
            recyclerclient.scrollToPosition(realmResults.size()-1);
            sizeOfPedidos = realmResults.size();

        }
        sizeOfPedidos = realmResults.size();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener= (OnFragmentInteractionListener)context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        // TODO: Update argument type and name
        void onFragmentInteraction(int id);
    }
}
